package com.josecognizant.ctc;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.josecognizant.ctc.util.Utilities;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.models.Tweet;

import java.util.List;

/**
 * Custom adapter to fill items of a RecyclerView responsible to show a timeline
 * Created by Jose on 09/06/2016.
 */
public class TweetAdapter extends RecyclerView.Adapter<TweetAdapter.TweetViewHolder> {
    private static final int EMPTY_TYPE = 0;
    private static final int NORMAL_TYPE = 1;
    private static OnRecyclerViewClickListener sItemClickListener;
    private List<Tweet> mDataSet;
    private Context mContext;

    public TweetAdapter(OnRecyclerViewClickListener listener, List<Tweet> tweets, Context context) {
        mDataSet = tweets;
        sItemClickListener = listener;
        mContext = context;
    }

    @Override
    public TweetViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutId = -1;
        if (viewType == EMPTY_TYPE) {
            layoutId = R.layout.timeline_empty;
        } else if (viewType == NORMAL_TYPE) {
            layoutId = R.layout.timeline_item;
        }
        View itemView = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
        return new TweetViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TweetViewHolder holder, int position) {
        if (holder.getItemViewType() == NORMAL_TYPE) {
            holder.onBind(mDataSet.get(position));
        }
    }

    @Override
    public int getItemViewType(final int position) {
        return (mDataSet.size() == 0) ? EMPTY_TYPE : NORMAL_TYPE;
    }

    @Override
    public int getItemCount() {
        if (mDataSet.size() == 0) {
            return 1;
        } else {
            return mDataSet.size();
        }
    }

    public void changeDataSet(List<Tweet> newTweetList) {
        if (newTweetList != null) {
            mDataSet = newTweetList;
            notifyDataSetChanged();
        }
    }

    public interface OnRecyclerViewClickListener {
        void onClick(View view, int position);
    }

    public class TweetViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        protected ImageView mItemImage;
        protected TextView mTweetRetweet;
        protected ImageView mRetweetImage;
        protected TextView mUserName;
        protected TextView mTweetTime;
        protected TextView mUserUserName;
        protected TextView mItemContent;
        protected TextView mRetweetNum;
        protected TextView mFavoriteNum;


        public TweetViewHolder(View itemView) {
            super(itemView);
            mItemImage = (ImageView) itemView.findViewById(R.id.list_item_image);
            mTweetRetweet = (TextView) itemView.findViewById(R.id.tweet_retweet);
            mRetweetImage = (ImageView) itemView.findViewById(R.id.list_item_tweet_ret_image);
            mUserName = (TextView) itemView.findViewById(R.id.user_name);
            mTweetTime = (TextView) itemView.findViewById(R.id.tweet_time);
            mUserUserName = (TextView) itemView.findViewById(R.id.user_username);
            mItemContent = (TextView) itemView.findViewById(R.id.list_item_content);
            mRetweetNum = (TextView) itemView.findViewById(R.id.num_retweets);
            mFavoriteNum = (TextView) itemView.findViewById(R.id.num_favorites);

            itemView.setOnClickListener(this);
        }

        public void onBind(Tweet tweet) {
            setMainTweetValues(tweet);
            if (tweet.retweetedStatus != null) {
                mRetweetImage.setVisibility(View.VISIBLE);
                mTweetRetweet.setVisibility(View.VISIBLE);
                if (tweet.user.id == Twitter.getSessionManager().getActiveSession().getUserId()) {
                    mTweetRetweet.setText(mContext.getString(R.string.retweeted_by_you));
                } else {
                    mTweetRetweet.setText(String.format(mContext.getString(R.string.retweeted_by_other), tweet.user.name));
                }
            } else {
                mRetweetImage.setVisibility(View.GONE);
                mTweetRetweet.setVisibility(View.GONE);
            }
        }

        private void setMainTweetValues(Tweet tweet) {
            Picasso.with(mContext)
                    .load(tweet.user.profileImageUrl)
                    .into(mItemImage);

            mTweetTime.setText(Utilities.getDate(tweet.createdAt));

            mItemContent.setText(tweet.text);
            mUserUserName.setText(String.format("@%s", tweet.user.screenName));
            mUserName.setText(tweet.user.name);

            mRetweetNum.setText(String.valueOf(tweet.retweetCount));
            mFavoriteNum.setText(String.valueOf(tweet.favoriteCount));
        }

        @Override
        public void onClick(View v) {
            sItemClickListener.onClick(v, getAdapterPosition());
        }
    }
}
