package com.josecognizant.ctc;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.josecognizant.ctc.util.Utilities;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.MediaEntity;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetui.TweetUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailTweetActivity extends AppCompatActivity {
    public static final String TWEET_ID_TO_SHOW = "tweet_id_to_show";


    @BindView(R.id.list_item_image)
    ImageView mTweetImage;
    @BindView(R.id.list_item_time)
    TextView mTweetDate;
    @BindView(R.id.list_item_content)
    TextView mTweetText;
    @BindView(R.id.list_item_user_username)
    TextView mTweetUsername;
    @BindView(R.id.list_item_user_name)
    TextView mUser;
    @BindView(R.id.list_item_num_ret)
    TextView mRetweetCount;
    @BindView(R.id.list_item_num_fav)
    TextView mFavCount;

    @BindView(R.id.imageDetail)
    ImageView mImageDetail;
    @BindView(R.id.cardImageDetail)
    CardView mCardImageDetail;

    @BindView(R.id.list_item_tweet_ret)
    TextView mRetweetText;
    @BindView(R.id.list_item_tweet_ret_image)
    ImageView mRetweetImage;


    private Tweet mTweet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_tweet);
        initToolbar();
        initFAB();
        getTweetFromIntent();
        ButterKnife.bind(this);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initFAB() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // TODO: Implement the reply functionality
                    Snackbar.make(view, R.string.snackbar_message_reply, Snackbar.LENGTH_LONG)
                            .show();
                }
            });
        }
    }

    private void getTweetFromIntent() {
        if (getIntent().hasExtra(TWEET_ID_TO_SHOW)) {
            long tweetId = getIntent().getLongExtra(TWEET_ID_TO_SHOW, -1);
            if (tweetId > 0) {
                TweetUtils.loadTweet(tweetId, new Callback<Tweet>() {
                    @Override
                    public void success(Result<Tweet> result) {
                        mTweet = result.data;
                        if (mTweet != null) {
                            initUIValues();
                        } else {
                            showErrorMessage();
                        }
                    }

                    @Override
                    public void failure(TwitterException exception) {
                        mTweet = null;
                    }
                });
            }
        }
    }

    private void initUIValues() {
        Picasso.with(this)
                .load(mTweet.user.profileImageUrlHttps)
                .into(mTweetImage);

        mTweetDate.setText(Utilities.getDate(mTweet.createdAt));
        mTweetText.setText(mTweet.text);
        mTweetUsername.setText(String.format("@%s", mTweet.user.screenName));
        mUser.setText(mTweet.user.name);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle(mTweet.user.name);
        }

        mRetweetCount.setText(String.valueOf(mTweet.retweetCount));
        mFavCount.setText(String.valueOf(mTweet.favoriteCount));

        if (mTweet.entities != null) {
            initUIEntitiesValues();
        }

        if (mTweet.retweetedStatus != null) {
            initRetweetValues();
        } else {
            mRetweetImage.setVisibility(View.GONE);
            mRetweetText.setVisibility(View.GONE);
        }
    }

    private void initRetweetValues() {
        if (mTweet.retweetedStatus != null) {
            mRetweetImage.setVisibility(View.VISIBLE);
            mRetweetText.setVisibility(View.VISIBLE);
            if (mTweet.user.getId() == Twitter.getSessionManager().getActiveSession().getUserId()) {
                mRetweetText.setText(getString(R.string.retweeted_by_you));
            } else {
                mRetweetText.setText(String.format(getString(R.string.retweeted_by_other), mTweet.user.name));
            }
        }
    }

    private void initUIEntitiesValues() {
        if (mTweet.entities.media != null && mTweet.entities.media.size() > 0) {
            MediaEntity mediaEntity = mTweet.entities.media.get(0);
            Picasso.with(this)
                    .load(mediaEntity.mediaUrlHttps)
                    .into(mImageDetail);
            mCardImageDetail.setVisibility(View.VISIBLE);
        } else {
            mCardImageDetail.setVisibility(View.GONE);
        }
    }

    private void showErrorMessage() {
        Toast.makeText(DetailTweetActivity.this, R.string.error_loading_tweet_details, Toast.LENGTH_SHORT).show();
    }
}
