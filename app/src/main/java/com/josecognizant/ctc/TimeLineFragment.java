package com.josecognizant.ctc;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.twitter.sdk.android.core.AppSession;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.Tweet;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment to hold the timeline of the app
 */
public class TimeLineFragment extends Fragment
        implements SwipeRefreshLayout.OnRefreshListener, TweetAdapter.OnRecyclerViewClickListener {

    private static final int NUMBER_MAX_TWEETS = 200;
    private static long sUserId = -1;
    private SwipeRefreshLayout mSwipeLayout;
    private TweetAdapter mAdapter;
    private List<Tweet> mTweetList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_timeline, container, false);
        initSwipeLayout(rootView);
        initAdapter();
        initRecyclerView(rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();
        if (session != null) {
            sUserId = session.getUserId();
            if (sUserId > 0) {
                onRefresh();
            }
        }
    }

    @Override
    public void onRefresh() {
        mSwipeLayout.setRefreshing(true);
        refreshListOfTweets();
    }

    @Override
    public void onClick(View view, int position) {
        startTweetDetailActivity(mTweetList.get(position).getId());
    }

    private void initAdapter() {
        mTweetList = new ArrayList<>();
        mAdapter = new TweetAdapter(this, mTweetList, getActivity());
    }

    private void initRecyclerView(View rootView) {
        RecyclerView recyclerTimeline = (RecyclerView) rootView.findViewById(R.id.recycler_timeline);
        recyclerTimeline.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerTimeline.setLayoutManager(layoutManager);
        recyclerTimeline.setAdapter(mAdapter);
    }

    private void initSwipeLayout(View rootView) {
        mSwipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);
        mSwipeLayout.setOnRefreshListener(this);
        mSwipeLayout.setColorSchemeResources(android.R.color.holo_blue_dark,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    private void refreshListOfTweets() {
        TwitterCore.getInstance().logInGuest(new Callback<AppSession>() {
            @Override
            public void success(Result<AppSession> result) {
                AppSession session = result.data;
                TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient(session);
                twitterApiClient.getStatusesService()
                        .userTimeline(sUserId, null, NUMBER_MAX_TWEETS, null, null, false, false, false, true,
                                new Callback<List<Tweet>>() {
                                    @Override
                                    public void success(Result<List<Tweet>> listResult) {
                                        mTweetList = listResult.data;
                                        mAdapter.changeDataSet(mTweetList);
                                        mSwipeLayout.setRefreshing(false);
                                    }

                                    @Override
                                    public void failure(TwitterException e) {
                                        e.printStackTrace();
                                        mSwipeLayout.setRefreshing(false);
                                    }
                                });
            }

            @Override
            public void failure(TwitterException e) {
                mSwipeLayout.setRefreshing(false);
            }
        });
    }

    private void startTweetDetailActivity(long tweetId) {
        Intent intent = new Intent(getActivity(), DetailTweetActivity.class);
        intent.putExtra(DetailTweetActivity.TWEET_ID_TO_SHOW, tweetId);
        startActivity(intent);
    }
}
