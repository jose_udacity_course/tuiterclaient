package com.josecognizant.ctc.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Utilities used in this twitter client app
 * Created by Jose on 10/06/2016.
 */
public class Utilities {
    private static final int DAY_IN_MILLIS = 24 * 60 * 60 * 1000;

    public static String getDate(final String dateString) {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZZ yyyy", Locale.getDefault());
        Date dateTweet;
        try {
            dateTweet = sdf.parse(dateString);
            Date yesterday = Utilities.getYesterdayDate();

            if (dateTweet.after(yesterday)) {

                return Utilities.getDifferenceInHours(new Date(), dateTweet);
            } else {
                SimpleDateFormat sdfFinal = new SimpleDateFormat("dd/MM/yy", Locale.getDefault());
                return sdfFinal.format(dateTweet);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Date getYesterdayDate() {
        return new Date(System.currentTimeMillis() - DAY_IN_MILLIS);
    }

    private static String getDifferenceInHours(final Date date, final Date previousDate) {
        long different = date.getTime() - previousDate.getTime();


        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;
        if (elapsedDays > 0)
            return elapsedDays + " d";

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;
        if (elapsedHours > 0)
            return elapsedHours + " h";

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;
        if (elapsedMinutes > 0)
            return elapsedMinutes + " m";

        long elapsedSeconds = different / secondsInMilli;


        return elapsedSeconds + " s";
    }
}
